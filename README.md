# cakeACP                                    
[![cakeACP](https://radeff.red/pics/acp/cakephpAcp.png)](cakeACP)

## PROJECT CLOSED
This project has been closed: see now [ACP Admin](https://github.com/acp-admin/acp-admin)

## Français
Logiciel de gestion pour une coopérative d'agriculture contractuelle de proximité (ACP en Suisse, AMAP en France)

Ce dépôt comprend les outils informatiques de gestion d'une coopérative agricole de proximité

- gestion des points de distribution
- gestion des demies-journées de travail
- gestion des produits sur commande
- ERP (Progiciel de gestion intégré - Dolibarr) adapté pour une ACP
- une passerelle CMS (Système de gestion de contenu - concrete5)
- un outil global d'administration (cakePhp)
- un système de commande de produits (panier électronique)

## English
Software for administration of a contractual of a contract farming organisation

